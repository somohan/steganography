<?php
namespace Steganography;
/**
 * This encoder will insert X extra characters spread evenly across the whole text.
 * It will look like the author of the text did not care about spelling errors at all.
 * This kind of sucks, but maybe it is good enough for some cases.
 * It is a two way encoding algorithm, which can conceal numbers in a text.
 */
class InsertCharNumberEncoder implements ITwoWayNumberEncoder {
	private static $dictionary = [0 => 'e', 1 => 't', 2 => 'r', 3 => 'o', 4 => 'n',
		5 => 's', 6 => 'd', 7 => 'i', 8 => 'g', 9 => 'a'];

	public function encode(int $secret, string $text) : string {
		if (!is_numeric($secret)) {
			throw new \InvalidArgumentException('We only support numeric secrets as of now');
		}

		$length = strlen($secret);
		// The distance in characters between each key char
		$keyCharDistance = (int)floor((strlen($text) - 5) / $length);
		$encodedSecret = self::encodeNumber($secret);
		foreach (str_split($encodedSecret) as $idx => $char) {
			$keyCharPosition = ($idx+1) * $keyCharDistance;
			#echo "$keyCharPosition: $char" . PHP_EOL;
			$text = self::insertChar($text, $keyCharPosition, $char);
		}
		return $text . ' ' . self::encodeNumber($length);
	}

	public function decode(string $text) : string {
		/**
		 * The last word holds the length of the secret - this is required, because that holds
		 * the key to figuring out how many chars there are between each key char.
		 * @todo This really sucks - could we calculate this or at least hide the info better in the text?
		 */
		$length = self::decodeString(substr($text, strrpos($text, ' ') + 1));

		// The distance in characters between each key char
		$keyCharDistance = (int)floor((strlen($text) - $length - 5) / $length);
		$encodedString = '';
		for ($i = 1; $i <= $length; $i++) {
			$keyCharPosition = $i * $keyCharDistance;
			$char = substr($text, $keyCharPosition, 1);
			$encodedString .= $char;
			#echo "$keyCharPosition: $char" . PHP_EOL;
		}
		return self::decodeString($encodedString);
	}

	private static function encodeNumber(int $number) : string {
		return self::translate(self::$dictionary, $number);
	}

	private static function decodeString(string $encoded) : int {
		$dictionary = array_flip(self::$dictionary);
		return self::translate($dictionary, $encoded);
	}

	/**
	 * Used for both encoding/decoding.
	 * @param array $dictionary
	 * @param string|integer $input
	 * @return string|integer
	 */
	private static function translate(array $dictionary, $input) {
		$output = '';
		foreach (str_split($input) as $char) {
			$output .= $dictionary[$char];
		}
		return is_numeric($output) ? (int)$output : $output;
	}

	private function insertChar($text, $position, $char) {
		return substr($text, 0, $position) . $char . substr($text, $position);
	}
}