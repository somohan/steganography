<?php
namespace Steganography;
interface ITwoWayNumberEncoder {
	public function encode(int $secret, string $text) : string;
	public function decode(string $text) : string;
}