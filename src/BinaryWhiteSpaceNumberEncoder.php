<?php
namespace Steganography;
/**
 * This is a two way number encoder, which will use the white space characters to hold our secret data.
 * Every white space character in the original text will correspond a bit:
 * - A single whitespace character means 0
 * - A double whitespace character means 1
 */
class BinaryWhiteSpaceNumberEncoder implements ITwoWayNumberEncoder {
	public function canEncodeNumber(int $secret, string $text) {
		// Make sure that we don't have any double spaces already, cause that's what we're encoding our secret
		$text = str_replace('  ', ' ', $text);
		// Now replace all spaces with a placeholder
		$text = str_replace(' ', '_BIT_', $text);
		// Count the number of spaces in the text to see if it is has enough spaces to hold our secret in binary notation
		$spaceCount = substr_count($text, '_BIT_');
		return $spaceCount >= $this->getRequiredSpaceCount($secret);
	}

	public function getRequiredSpaceCount(int $secret) : int {
		return strlen(decbin($secret));
	}

	public function encode(int $secret, string $text) : string {
		// Make sure that we don't have any double spaces already, cause that's what we're encoding our secret
		$text = str_replace('  ', ' ', $text);
		// Now replace all spaces with a placeholder
		$text = str_replace(' ', '_BIT_', $text);
		// Convert the secret to binary
		$binary = decbin($secret);
		// Calculate the number of bits in the binary
		$binaryLength = strlen($binary);
		// Count the number of spaces in the text to see if it is has enough spaces to hold our secret in binary notation
		$spaceCount = substr_count($text, '_BIT_');
		if ($spaceCount < $binaryLength) {
			throw new \InvalidArgumentException("The text is too short to conceal the secret number - too few white space characters: $spaceCount. $binaryLength was needed.");
		}

		// Left pad the binary with zeroes, so it matches the number of spaces in the text
		$binary = str_pad($binary, $spaceCount, '0', STR_PAD_LEFT);
		// Split it in bits
		$bits = str_split($binary);
		// Split the text in words
		$words = explode('_BIT_', $text);
		// Re-assemble the text with double spaces for every 1-bit
		$out = '';
		foreach ($words as $i => $word) {
			$out .= $word;
			if (array_key_exists($i, $bits)) {
				$out .= $bits[$i] == 0 ? ' ' : '  ';
			}
		}
		return $out;
	}

	public function decode(string $text) : string {
		$text = str_replace(' ', '_0BIT_', $text);
		$text = str_replace('_0BIT__0BIT_', '_1BIT_', $text);
		preg_match_all('/_([0-1]+)BIT_/', $text, $matches);
		return bindec(implode('', $matches[1]));
	}
}