# README #

A library for dealing with steganography in PHP.
A features a number of different encoders, which can be used wherever.

### Testing ###

Run the test suite with the following command:

```
./vendor/bin/phpunit test/
```