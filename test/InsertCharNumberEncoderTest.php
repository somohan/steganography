<?php
namespace Steganography\Test;
use Steganography\InsertCharNumberEncoder;

class InsertCharEncoderTest extends BaseTestCase {
	public function test() {
		$encoder = new InsertCharNumberEncoder();
		$encoded = $encoder->encode($this->secretNumber, $this->text);
		$decoded = $encoder->decode($encoded);
		self::assertEquals($this->secretNumber, $decoded);
	}
}
