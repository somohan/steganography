<?php
namespace Steganography\Test;
use Steganography\BinaryWhiteSpaceNumberEncoder;

class BinaryWhiteSpaceEncoderTest extends BaseTestCase {
	public function testEncodeDecode() {
		$encoder = new BinaryWhiteSpaceNumberEncoder();
		$encoded = $encoder->encode($this->secretNumber, $this->text);
		$decoded = $encoder->decode($encoded);
		self::assertEquals($this->secretNumber, $decoded);
	}

	public function testGetRequiredSpaceCount() {
		$encoder = new BinaryWhiteSpaceNumberEncoder();
		self::assertEquals(32, $encoder->getRequiredSpaceCount($this->secretNumber));
	}

	public function testCanEncodeNumber() {
		$encoder = new BinaryWhiteSpaceNumberEncoder();
		self::assertTrue($encoder->canEncodeNumber($this->secretNumber, $this->text));
		self::assertFalse($encoder->canEncodeNumber($this->secretNumber, $this->tooShortText));
	}

	protected $tooShortText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id congue ex. Pellentesque vitae bibendum mi, quis iaculis felis. Praesent malesuada tellus dui, sit amet placerat quam vulputate id.';
}
