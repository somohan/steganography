<?php
namespace Steganography\Test;
use PHPUnit\Framework\TestCase;
abstract class BaseTestCase extends TestCase {
	protected $secretNumber = 4206403917;
	protected $text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id congue ex. Pellentesque vitae bibendum mi, quis iaculis felis. Praesent malesuada tellus dui, sit amet placerat quam vulputate id. Sed sed risus tortor. Mauris vel tincidunt tortor. Cras vitae ex.';
}